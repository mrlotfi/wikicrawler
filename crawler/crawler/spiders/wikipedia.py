# -*- coding: utf-8 -*-
import re
import urllib

import bs4
import scrapy
from bs4 import BeautifulSoup
import json

def print_progress(progressed, total):
  total_chars_used = 40
  if progressed == total:
    print('\r[{s}] %100\nDone!                               '.format(s=total_chars_used*'#'))
    return
  sharp_number = int(total_chars_used * (progressed / total))
  minus_number = total_chars_used - sharp_number
  print('\r[{sharps}{minus}] {numeric}%                         '.format(sharps='#'*sharp_number, minus='-'*minus_number, numeric=100*(progressed/total)), end='\r')

class WikipediaSpider(scrapy.Spider):
    name = "wikipedia"
    allowed_domains = ["fa.wikipedia.org"]
    base_url = "https://fa.wikipedia.org"

    def filter(self, link_href):
      # contains :
      if link_href.find(":") != -1:
        return False
      
      # contains بهام‌زدایی
      if link_href.find("بهام‌زدایی") != -1:
        return False
      # is edit link
      if link_href.find("action=edit") != -1:
        return False

      # contains numbers
      if re.search(r'\d', link_href):
        return False

      # check if it is an outsider link
      if not link_href.startswith(r'/wiki'):
        return False

      # remove #
      if link_href.find('#') != -1:
        link_href = link_href[:link_href.find('#')]
      if not link_href:
        return False
      return link_href

    def __init__(self, *args, **kwargs): 
      super(WikipediaSpider, self).__init__(*args, **kwargs) 
      # self.start_urls = kwargs.get('start_urls') 
      self.out_degree = kwargs['out_degree']
      self.total_pages = kwargs['total_pages']
      self.output_path = kwargs['output_path']
      self.crawled_pages = len(self.start_urls)
      self.parsed_pages = 0
      self.seenpages = set([url.replace('https://fa.wikipedia.org', '') for url in self.start_urls])

    def make_requests_from_url(self, url):
      return scrapy.Request(url, dont_filter=False)

    def parse(self, response):
        # progress and report progress
        self.parsed_pages += 1
        page_id = self.parsed_pages
        print_progress(self.parsed_pages, self.total_pages)

        text = response.text
        soup = BeautifulSoup(text, 'lxml')
        main_title = soup.find('h1', {'id': 'firstHeading'}).text
        
        main_box = soup.find('div', {'id': 'mw-content-text'})
        links = main_box.find_all('a')

        first_paragraph = main_box.find('p', recursive=False).get_text()

        # removing some kosshers
        unwanted_table_of_contents = main_box.find('div', {'id': 'toc'})
        unwanted_table_of_contents.extract() if unwanted_table_of_contents else None
        unwanted_references = main_box.find('ol', {'class': 'references'})
        unwanted_references.extract() if unwanted_references else None
        unwanted_navbars = main_box.find_all('table', {'class': 'navbox'})
        for item in unwanted_navbars:
          item.extract()
        unwanted_reference_links = main_box.find_all('sup', {'class': 'reference'})
        for item in unwanted_reference_links:
          item.extract()
        unwanted_sth = main_box.find('div', {'class': 'dablink'})
        unwanted_sth.extract() if unwanted_sth else None
        unwanted_edit_links = main_box.find_all('span', {'class': 'mw-editsection'})
        for item in unwanted_edit_links:
          item.extract()
        for script_tag in main_box.find('script'):
          script_tag.extract()
        main_box.find('table', {'class': 'infobox'}).extract() if main_box.find('table', {'class': 'infobox'}) else None
        for reflist_bar in main_box.find_all('div', {'class': 'reflist'}):
          reflist_bar.extract()
 
        # now kossher is removed we can extract the whole text
        text = main_box.get_text()

        # setting up links. total_out_links is links to other wiki pages. ordered_out_links is a list of unvisited fa wikipedia pages. the url is relative to fa.wikipedia.org
        total_out_links = []
        ordered_out_links = []
        for link in links:
          try:
            href = link['href']
            unquoted_href = urllib.parse.unquote(href)
            if self.filter(unquoted_href):
              total_out_links.append(unquoted_href)
              if not unquoted_href in self.seenpages:
                self.seenpages.add(unquoted_href)
                ordered_out_links.append(response.urljoin(unquoted_href))
          except:
            pass

        # produce output and insert it into files
        output_info =  {
          'url': urllib.parse.unquote(response.url),
          'title': main_title,
          'first_paragraph': first_paragraph,
          'text': text,
          'links': total_out_links
        }
        with open(self.output_path+'/'+str(page_id)+'.json', 'w') as output_file:
          json.dump(output_info, output_file)

        # just crawl if we want more pages
        for out_link in ordered_out_links[0:self.out_degree]:
          if self.crawled_pages < self.total_pages:        
            yield scrapy.Request(out_link, callback=self.parse, dont_filter=False)
            self.crawled_pages += 1
