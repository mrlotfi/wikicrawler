from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import os
# set this to crwaler root path on your disk (where scrapy.cfg is there)
crawler_path = '/mnt/Data/Workspace/term7/mir/project2/code/crawler'
os.chdir(crawler_path)
settings = get_project_settings()
process = CrawlerProcess(settings)
process.crawl('wikipedia', start_urls=['https://fa.wikipedia.org/wiki/سعدی'], out_degree=10, total_pages = 13, output_path = '/mnt/Data/Workspace/term7/mir/project2/code/generated_jsons')
process.start()
a = input('press enter to finish')

# os.remove('/mnt/Data/Workspace/term7/mir/project2/code/crawler/crawl_result.json')